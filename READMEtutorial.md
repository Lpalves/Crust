# LABGEO

Projeto CCI-DNIT, conhecido internamente como labgeo.

### Links úteis ###

* http://phpdoc.org/docs/latest/index.html (como documentar o código)
* http://php.net (site oficial do PHP)
* http://github.com/airbnb/javascript (boas práticas para javascript)
* http://mysql.com/ (site oficial do MySQL)
* https://getcomposer.org/ (component manager backend)

----------

### Requisitos Mínimos ###

* Apache **2.4.18+**
* PHP **7.0+**
* MySQL **5.7.11+**
* NodeJS **8.9.1+**
* Composer **1.5.1+**

----------


### Preparando o ambiente para o desenvolvimento ###

1) Instale o pacote apache/php/mysql:

* Windows : [wamp](http://www.wampserver.com/en/).
* Linux: [lamp](https://www.digitalocean.com/community/tutorials/how-to-install-lamp-on-ubuntu-14-04-quickstart).
* MacOS: [mamp](https://www.mamp.info/en/).

2) Após instalado e iniciado os serviços, acesse o phpmyadmin, comumente encontrado em http://localhost/phpmyadmin, digite usuário: root, com password: "root", vá no menu **importar** e importe o arquivo **database.sql** localizado na pasta **docs/** encontrada na raíz do projeto.

![Visão phpmyadmin](http://i.imgur.com/WpI96JL.png)

3) Ative a diretiva [mod_rewrite](http://phpcollection.com/2009/08/22/mod-rewrite-windows-wamp-xampp/).

4) Se você estiver usando WINDOWS, baixe o .exe do composer (https://getcomposer.org/download/) e adicione o php ao seu path de variáveis do sistema (https://john-dugan.com/add-php-windows-path-variable/)

5) No diretório do projeto, execute o comando `npm install`. Após essa operação, ele irá executar o `composer install`, seguido por `gulp build`, para gerar os arquivos estáticos do projeto.

6) Vá em `server/config/` e crie um arquivo `init.php` com formato igual à `init.example.php` e preencha as informações necessárias. Esse arquivo contém constantes de ambiente, e não devem ser enviadas para o repositório remoto.

7) Reinicie o wamp, se a instalação ocorreu sem erros, acesse o projeto em http://localhost. Você deverá ver a página inicial do projeto.

8) Parabéns, você instalou tudo certinho! Execute o comando `npm start` para desenvolver.


___PS: Caso queira usar o Gulp diretamente, instale ele globalmente com `npm install -g gulp`.___

----------

### Preparando para o deploy ###

1) Rode o comando "**npm run deploy**".

2) Faça upload da pasta **deploy**.

3) Caso for o primeiro upload, crie um arquivo **deploy/server/config/init.php**, ele não é gerado no deploy automático.

----------
Instruções por [Francisco Knebel](https://github.com/franciscoknebel).

15 de Novembro de 2017
